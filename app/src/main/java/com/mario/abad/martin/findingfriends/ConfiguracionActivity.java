package com.mario.abad.martin.findingfriends;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ConfiguracionActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private FirebaseAuth mAuth;
    private FirebaseDatabase basededatos ;
    private DatabaseReference ref;
    private DatosPerfil perfil;


    private Switch sBacalao;
    private Switch sPaella;
    private Switch sBici;
    private Switch sEverest;
    private Switch sTiburones;
    private Switch sQuijote;

    private String TAG = "Config";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

        mAuth = FirebaseAuth.getInstance();
        basededatos = FirebaseDatabase.getInstance();
        ref = basededatos.getReference();


        //Tomamos las referencias de los switches
        sBacalao = findViewById(R.id.switchBacalao);
        sPaella = findViewById(R.id.switchPaella);
        sBici = findViewById(R.id.switchBici);
        sEverest = findViewById(R.id.switchEverest);
        sTiburones = findViewById(R.id.switchTiburones);
        sQuijote = findViewById(R.id.switchQuijote);

        //Obtenemos el objeto DatosPerfil con la información del usuario
        perfil = (DatosPerfil) getIntent().getSerializableExtra("miperfil");

        //Establecemos los valores de los switches
        sBacalao.setChecked(perfil.getGustaMusicaBacalao());
        sPaella.setChecked(perfil.getGustaPaella());
        sBici.setChecked(perfil.getGustaBici());
        sEverest.setChecked(perfil.getEscalarEverest());
        sTiburones.setChecked(perfil.getBucearConTiburones());
        sQuijote.setChecked(perfil.getLeerElQuijote());

        //Añadimos listener a los switches para poder recoger los cambios
        sBacalao.setOnCheckedChangeListener(this);
        sPaella.setOnCheckedChangeListener(this);
        sBici.setOnCheckedChangeListener(this);
        sEverest.setOnCheckedChangeListener(this);
        sTiburones.setOnCheckedChangeListener(this);
        sQuijote.setOnCheckedChangeListener(this);

        //Tomamos referencia al botón para pasar a la pantalla de Información personal
        Button detalleButton = (Button)findViewById(R.id.detalleBot);
        detalleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfiguracionActivity.this, DetalleActivity.class);
                intent.putExtra("miperfil", perfil);
                startActivityForResult(intent, 2);


            }
        });


        //Tomamos referencia al botón eliminar usuario
        Button eliminarUsuarioBoton = (Button)findViewById(R.id.eliminarBoton);
        eliminarUsuarioBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(ConfiguracionActivity.this).create();
                alertDialog.setTitle("Atención: Borrado de usuario");
                alertDialog.setMessage("Esta acción eliminará al usuario ¿continuar?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Adelante. Borrar usuario",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                //Establecemos oyente para borrar los datos de la bbdd si se desconecta
                                DatabaseReference refConn = basededatos.getReference().child("users").child(mAuth.getCurrentUser().getUid());
                                refConn.removeValue();

                                user.delete()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d(TAG, "User account deleted.");

                                                    //FirebaseAuth.getInstance().signOut();
                                                    Log.d("Select","Botón desconectar pulsado");


                                                    Intent intent = new Intent(ConfiguracionActivity.this, MainActivity.class);
                                                    startActivity(intent);


                                                } else {

                                                    Log.d(TAG, "No se ha podido borrar");

                                                    AlertDialog alertDialog = new AlertDialog.Builder(ConfiguracionActivity.this).create();
                                                    alertDialog.setTitle("Debes reiniciar sesión");
                                                    alertDialog.setMessage("Esta acción requiere que te vuelvas a validar");
                                                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    dialog.dismiss();
                                                                    FirebaseAuth.getInstance().signOut();
                                                                    Log.d("Select","Botón desconectar pulsado");


                                                                    Intent intent = new Intent(ConfiguracionActivity.this, MainActivity.class);
                                                                    startActivity(intent);
                                                                }
                                                            });

                                                    alertDialog.show();
                                                }
                                            }
                                        });

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();


            }
        });

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        String campo = null;

        Log.d("Checked:", String.valueOf(findViewById(buttonView.getId())));

        if (buttonView == sBacalao) {
            perfil.setGustaMusicaBacalao(isChecked);
            campo = "bacalao_music";
        } else if (buttonView == sPaella) {
            perfil.setGustaPaella(isChecked);
            campo = "paella";

        } else if (buttonView == sBici) {
            perfil.setGustaBici(isChecked);
            campo = "montar_bici";

        } else if (buttonView == sEverest) {
            perfil.setEscalarEverest(isChecked);
            campo = "escalar_everest";

        } else if (buttonView == sTiburones) {
            perfil.setBucearConTiburones(isChecked);
            campo = "bucear_tiburones";

        } else if (buttonView == sQuijote) {
            perfil.setLeerElQuijote(isChecked);
            campo = "leer_quijote";

        }

        actualizarBaseDatos(campo,isChecked);

    }

    public void actualizarBaseDatos(String campo, boolean dato){


       //DatabaseReference usuariosRef = basededatos.getReference("users").child(mAuth.getCurrentUser().getUid().toString());
       //usuariosRef.child(campo).setValue(dato);
        ref.child("users").child(mAuth.getCurrentUser().getUid()).child(campo).setValue(dato);

    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("perfilResult",perfil);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();

        super.onBackPressed();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){

                perfil = (DatosPerfil) data.getSerializableExtra("perfilResult");

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

}
