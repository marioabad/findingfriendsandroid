package com.mario.abad.martin.findingfriends;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference usuariosRef;
    private FirebaseDatabase basededatos = FirebaseDatabase.getInstance();

    public ProgressDialog mProgressDialog;

    public DatosPerfil perfil;

    private static final String TAG = "EmailPassword";

    EditText usuarioTxt;
    EditText contrasenaTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usuarioTxt = findViewById(R.id.usuarioText);
        contrasenaTxt = findViewById(R.id.contrasenaText);

        mAuth = FirebaseAuth.getInstance();


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("TAG", "onAuthStateChanged:signed_in:" + user.getUid());

                    irALaAplicacion();


                } else {
                    // User is signed out
                    Log.d("TAG", "onAuthStateChanged:signed_out");

                    Button validarBoton = findViewById(R.id.validarBot);
                    validarBoton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            signIn(usuarioTxt.getText().toString(), contrasenaTxt.getText().toString());
                            //signIn("marioabadm@gmail.com", "123456");
                            Log.d("TAG", "Usuario:" + usuarioTxt.getText().toString() + " " + contrasenaTxt.getText().toString());

                        }
                    });
                }
                // ...
            }
        };

        //No mostrar teclado al principio
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

}

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            irALaAplicacion();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        if (!task.isSuccessful()) {
                            //mStatusTextView.setText(R.string.auth_failed);
                        }
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    private boolean validateForm() {
        boolean valido = true;

        String email = usuarioTxt.getText().toString();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(MainActivity.this, "Debes rellenar un usuario",
                    Toast.LENGTH_SHORT).show();
            valido = false;
        }

        String password = contrasenaTxt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(MainActivity.this, "Debes poner la contraseña",
                    Toast.LENGTH_SHORT).show();
            valido = false;
        }

        return valido;
    }


    public void irALaAplicacion(){

        showProgressDialog();

        //Cargamos el perfil de usuario con los datos de BBDD
        usuariosRef = basededatos.getReference("users").child(mAuth.getCurrentUser().getUid());
        usuariosRef.addListenerForSingleValueEvent(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                HashMap<String, Object> value = (HashMap<String, Object>) dataSnapshot.getValue();

                if (value == null) {

                } else {
                    hideProgressDialog();

                    Log.d(TAG, "Value is: " + value);
                    Log.d(TAG, "Género: " + value.get("genero"));


                    try {

                        perfil = new DatosPerfil(value.get("email").toString(),value.get("nickname").toString(),value.get("genero").toString(),
                                value.get("fec_nacimiento").toString(),value.get("ciudad").toString(),Boolean.valueOf(value.get("bacalao_music").toString()),
                                Boolean.valueOf(value.get("paella").toString()),Boolean.valueOf(value.get("montar_bici").toString()),
                                Boolean.valueOf(value.get("escalar_everest").toString()), Boolean.valueOf(value.get("bucear_tiburones").toString()),
                                Boolean.valueOf(value.get("leer_quijote").toString()));


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Objeto creado " + perfil.getCiudad());

                    Intent intent = new Intent(MainActivity.this, PrincipalActivity.class);
                    intent.putExtra("Usuario", usuarioTxt.getText().toString());
                    intent.putExtra("miperfil", perfil);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Error");
            }
        });
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Cargando");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void irAAltaUsuario(View view){
        Intent intent = new Intent(MainActivity.this, AltaUsuarioActivity.class);
        startActivity(intent);
    }
}