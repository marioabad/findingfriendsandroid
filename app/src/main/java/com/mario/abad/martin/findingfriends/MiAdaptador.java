package com.mario.abad.martin.findingfriends;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import java.util.ArrayList;
import java.util.List;

public class MiAdaptador extends BaseAdapter {


        private Context context;
        private ArrayList<List<String>> lineasArray;

        public MiAdaptador(Context context, ArrayList<List<String>> lineasArray) {
            this.context = context;
            this.lineasArray = lineasArray;
        }

        @Override
        public int getCount() {
            return lineasArray.size();
        }

        @Override
        public Object getItem(int position) {
            return lineasArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            TwoLineListItem twoLineListItem;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                twoLineListItem = (TwoLineListItem) inflater.inflate(
                        android.R.layout.simple_list_item_2, null);
            } else {
                twoLineListItem = (TwoLineListItem) convertView;
            }

            TextView text1 = twoLineListItem.getText1();
            TextView text2 = twoLineListItem.getText2();

            text1.setTextSize(0,35);
            text2.setTextSize(0,35);

            text1.setText(lineasArray.get(position).get(0));
            text2.setText("" + lineasArray.get(position).get(1));

            return twoLineListItem;
        }

}
