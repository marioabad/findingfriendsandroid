package com.mario.abad.martin.findingfriends;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class AltaUsuarioActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference usuariosRef;
    private FirebaseDatabase basededatos = FirebaseDatabase.getInstance();

    public ProgressDialog mProgressDialog;

    public DatosPerfil perfil;

    private static final String TAG = "EmailPassword";

    EditText usuarioTxt;
    EditText contrasenaTxt;
    EditText nickTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta_usuario);

        usuarioTxt = findViewById(R.id.usuarioText);
        contrasenaTxt = findViewById(R.id.contrasenaText);
        nickTxt = findViewById(R.id.nickText);

        mAuth = FirebaseAuth.getInstance();

        //No mostrar teclado al principio
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }



    private void createAccount(final String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Si Ok
                            //Log.d(TAG, "createUserWithEmail:success");
                            //FirebaseUser user = mAuth.getCurrentUser();
                            irALaAplicacion(email,nickTxt.getText().toString());
                        } else {
                            //Si falla, mostrar mensaje
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(AltaUsuarioActivity.this, "Ocurrió un error al crear cuenta",
                                    Toast.LENGTH_SHORT).show();
                        }

                        hideProgressDialog();

                    }
                });
    }

    private boolean validateForm() {
        boolean valido = true;

        String email = usuarioTxt.getText().toString();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(AltaUsuarioActivity.this, "Debes rellenar el email",
                    Toast.LENGTH_SHORT).show();
            valido = false;
        }

        String password = contrasenaTxt.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(AltaUsuarioActivity.this, "Debes poner una contraseña",
                    Toast.LENGTH_SHORT).show();
            valido = false;
        }

        String nick = nickTxt.getText().toString();
        if (TextUtils.isEmpty(nick)) {
            Toast.makeText(AltaUsuarioActivity.this, "Debes poner una Nick Name",
                    Toast.LENGTH_SHORT).show();
            valido = false;
        }

        return valido;
    }

    private void irALaAplicacion(String email, String nick) {

        showProgressDialog();

        try {
            perfil = new DatosPerfil(email, nick);
        } catch (ParseException e) {
                e.printStackTrace();
        }


        establecerDatos(email, nick);

        System.out.println("Objeto creado " + perfil.getCiudad());
        Intent intent = new Intent(AltaUsuarioActivity.this, PrincipalActivity.class);
        intent.putExtra("Usuario", usuarioTxt.getText().toString());
        intent.putExtra("miperfil", perfil);
        startActivity(intent);

        hideProgressDialog();
    }

    public void establecerDatos(final String email, final String nick){
        // Guardamos los cambios en la base de datos
        // Creamos HashMap y lo poblamos con los datos cambiados
        Map<String, Object> datosUsuario = new HashMap<String, Object>() {{
            put("email", email);
            put("nickname", nick);
            put("genero", "Hombre");
            put("fec_nacimiento", "01/01/1975");
            put("ciudad", "Madrid");
            put("bacalao_music", false);
            put("paella", false);
            put("montar_bici", false);
            put("escalar_everest", false);
            put("bucear_tiburones", false);
            put("leer_quijote", false);
        }};

        DatabaseReference ref = basededatos.getReference();;
        // Actualizamos Base de Datos
        ref.child("users").child(mAuth.getCurrentUser().getUid()).setValue(datosUsuario);
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Cargando");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void darAlta(View v) {

        createAccount(usuarioTxt.getText().toString(), contrasenaTxt.getText().toString());

    }
}
