package com.mario.abad.martin.findingfriends;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrincipalActivity extends AppCompatActivity {

    //private FirebaseAuth mAuth;
    private FirebaseUser user;
    private DatosPerfil perfil;
    private TextView usuarioTxt;
    private TextView datosLoc;

    private static final String TAG = "BaseDeDatos";
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;

    private FirebaseAuth mAuth;
    private FirebaseDatabase basededatos = FirebaseDatabase.getInstance();
    private String miLocalizacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        mAuth = FirebaseAuth.getInstance();

        datosLoc = findViewById(R.id.tvDatos);
        datosLoc.setText("Intentando localizarte, aún tus amigos no te pueden localizar...");

        user = mAuth.getCurrentUser();
        if (user != null) {
            usuarioTxt = (TextView)findViewById(R.id.usuarioText);

            perfil = (DatosPerfil) getIntent().getSerializableExtra("miperfil");
            usuarioTxt.setText(perfil.getEmail() + " " + perfil.getCiudad());

        } else {
            finish();
        }

        startCapturingLocation();

    }


    public void startCapturingLocation(){
        // Iniciamos la captura de posición

        if (checkPermission()){

            LocationManager locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,0,locationListener);

        } else {
            requestPermissions();
        }


    }

    public void irALocAmi(View view){

        Intent intent = new Intent(PrincipalActivity.this, MapaActivity.class);
        intent.putExtra("miperfil", perfil);
        startActivity(intent);

    }


    public void irAConfBot(View view){
        Intent intent = new Intent(PrincipalActivity.this, ConfiguracionActivity.class);
        intent.putExtra("miperfil", perfil);
        startActivityForResult(intent, 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return  true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //Establecemos oyente para borrar los datos de la bbdd si se desconecta
        DatabaseReference refConn = basededatos.getReference().child("usersConnected").child(mAuth.getCurrentUser().getUid());
        refConn.removeValue();


        FirebaseAuth.getInstance().signOut();
        Log.d("Select","Botón desconectar pulsado");

        user = null;

        Intent intent = new Intent(PrincipalActivity.this, MainActivity.class);
        startActivity(intent);


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){

                perfil = (DatosPerfil) data.getSerializableExtra("perfilResult");

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

    // Comprobamos que tiene permisos para localización
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(PrincipalActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private void requestPermissions(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            Toast.makeText(this,"La aplicación necesita permisos de GPS para obtener tu posición",Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Snackbar.make(datosLoc, "Permiso concedido, ahora tus amigos te pueden encontrar",Snackbar.LENGTH_LONG).show();

                } else {
                    Snackbar.make(datosLoc, "Permiso denegado, tus amigos no te pueden encontrar",Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    LocationListener locationListener = new LocationListener() {

        public void onLocationChanged(final Location location) {
            String msg = "Te hemos localizado: longitud: %s latitud: %s altitud: %s";
            msg = String.format(msg, location.getLongitude(), location.getLatitude(), location.getAltitude());
            try {
                msg += "\nCalle: " + getAddressFromLocation(location.getLatitude(), location.getLongitude());
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (miLocalizacion == null){

                miLocalizacion = "%s,%s";
                miLocalizacion = String.format(miLocalizacion,location.getLatitude(),location.getLongitude());


                DatabaseReference ref = basededatos.getReference().child("usersConnected");

                Map<String, String> datosUsuarioCon = new HashMap<String, String>() {{

                    put("nickname", perfil.getNickName());
                    put("position", miLocalizacion);
                }};
                // Actualizamos Base de Datos
                ref.child(mAuth.getCurrentUser().getUid()).setValue(datosUsuarioCon);

                //Establecemos oyente para borrar los datos de la bbdd si se desconecta
                DatabaseReference refConn = basededatos.getReference().child("usersConnected").child(mAuth.getCurrentUser().getUid());
                refConn.onDisconnect().removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError error, DatabaseReference firebase) {
                        if (error != null) {
                            System.out.println("could not establish onDisconnect event:" + error.getMessage());
                        }
                    }
                });
            }


            datosLoc.setText(msg);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        public void onProviderEnabled(String provider) {
            Toast.makeText(PrincipalActivity.this, "Proveedor Habilitado", Toast.LENGTH_SHORT).show();
        }

        public void onProviderDisabled(String provider) {
            Toast.makeText(PrincipalActivity.this, "Proveedor Deshabilitado", Toast.LENGTH_SHORT).show();
        }
    };

    private String getAddressFromLocation(double latitud, double longitud) throws IOException {
        Geocoder geocoder = new Geocoder(this);
        List<Address> matches = geocoder.getFromLocation(latitud,longitud,1);
        if (matches.size() < 1 ) return "";
        return (matches.isEmpty() ? null : matches.get(0).getAddressLine(0));
    }

    @Override
    protected void onResume() {


        if (user != null) {
            Log.e(TAG, "Hay ususario" + user.getUid().toString());
            usuarioTxt.setText(perfil.getEmail() + " " + perfil.getCiudad());
            startCapturingLocation();

        } else {
            finish();
        }

        super.onResume();
    }
}

