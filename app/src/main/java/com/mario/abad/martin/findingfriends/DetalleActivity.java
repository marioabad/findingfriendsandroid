package com.mario.abad.martin.findingfriends;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class  DetalleActivity extends AppCompatActivity {

    private DatosPerfil perfil;
    private FirebaseAuth mAuth;
    private FirebaseDatabase basededatos ;
    private DatabaseReference ref;

    EditText mynick;
    Switch myGenero;
    DatePicker myDatePicker;
    Spinner myCiudad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        //No mostrar teclado al principio
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mAuth = FirebaseAuth.getInstance();
        basededatos = FirebaseDatabase.getInstance();
        ref = basededatos.getReference();

        //Obtenemos el objeto DatosPerfil con la información del usuario
        perfil = (DatosPerfil) getIntent().getSerializableExtra("miperfil");

        //Tomamos las referencias de los controles
        mynick = findViewById(R.id.nickET);
        myGenero = findViewById(R.id.generoSwitch);
        myDatePicker = findViewById(R.id.datepicker);
        myCiudad = findViewById(R.id.ciudadSpinner);

        //Establecemos los valores de los controles
        mynick.setText(perfil.getNickName());

        myGenero.setChecked(!perfil.getEsHombre());

        Calendar cal = Calendar.getInstance();
        cal.setTime(perfil.getFechaNac());
        int mesNac = cal.get(Calendar.MONTH);
        int anioNac= cal.get(Calendar.YEAR);
        int diaNac = cal.get(Calendar.DAY_OF_MONTH);
        myDatePicker.updateDate(anioNac,mesNac,diaNac);
        
        myCiudad.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Categorias.ciudades));

        int posicion = 0;
        for (int i=0; i<Categorias.ciudades.length; i++){
            if (perfil.getCiudad().equals(Categorias.ciudades[i])){
                posicion = i;
            }
        }

        myCiudad.setSelection(posicion,false);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.guardarmenu, menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        //Guardamos los cambios en el objeto DatosPerfil
        perfil.setNickName(mynick.getText().toString());
        perfil.setEsHombre(!myGenero.isChecked());

        //Convertimos fecha para incluir en la propiedad del objeto
        String string_date = myDatePicker.getYear() + "/" + (myDatePicker.getMonth()+1) + "/" + myDatePicker.getDayOfMonth();
        SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd");
        try {
            Date d = f.parse(string_date);
            //long milliseconds = d.getTime();
            perfil.setFechaNac(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        perfil.setCiudad(Categorias.ciudades[myCiudad.getSelectedItemPosition()]);

        // Guardamos los cambios en la base de datos
        // Creamos HashMap y lo poblamos con los datos cambiados
        Map<String, Object> datosUsuario = new HashMap<String, Object>() {{
            String string_date_paraBBDD = myDatePicker.getDayOfMonth() + "/" + (myDatePicker.getMonth()+1) + "/" + myDatePicker.getYear() ;
            put("nickname", perfil.getNickName());
            put("genero", myGenero.isChecked() ? "Mujer" : "Hombre");
            put("fec_nacimiento", string_date_paraBBDD);
            put("ciudad", perfil.getCiudad());
        }};

        // Actualizamos Base de Datos
        ref.child("users").child(mAuth.getCurrentUser().getUid()).updateChildren(datosUsuario);

        //Creamos intent y result para que lo con el objeto perfil para que lo recoja la Activity anterior
        Intent returnIntent = new Intent();
        returnIntent.putExtra("perfilResult",perfil);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();

        return super.onOptionsItemSelected(item);
    }
}
