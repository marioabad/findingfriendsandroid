package com.mario.abad.martin.findingfriends;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DatosPerfil implements Serializable{

    private String email;
    private String nickName;
    private Boolean esHombre;
    private Date fechaNac;
    private String ciudad;

    private Boolean gustaMusicaBacalao;
    private Boolean gustaPaella;
    private Boolean gustaBici;

    private Boolean escalarEverest;
    private Boolean bucearConTiburones;
    private Boolean leerElQuijote;


    public DatosPerfil(String email, String nickName) throws ParseException {
        this.email = email;
        this.nickName = nickName;
        this.esHombre = true;

        DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        this.fechaNac = format.parse("16/08/1975");
        this.ciudad = "Madrid";
        this.gustaMusicaBacalao = false;
        this.gustaPaella = false;
        this.gustaBici = false;
        this.escalarEverest = false;
        this.bucearConTiburones = false;
        this.leerElQuijote = false;
    }



    public DatosPerfil(String email, String nickName, String genero, String fechaNac, String ciudad,
                       Boolean gustaMusicaBacalao, Boolean gustaPaella, Boolean gustaBici, Boolean escalarEverest,
                       Boolean bucearConTiburones, Boolean leerElQuijote) throws ParseException {
        this.email = email;
        this.nickName = nickName;
        if (genero.equals("Hombre")){
            this.esHombre = true;
        } else {
            this.esHombre = false;
        }

        DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        this.fechaNac = format.parse(fechaNac);
        this.ciudad = ciudad;
        this.gustaMusicaBacalao = gustaMusicaBacalao;
        this.gustaPaella = gustaPaella;
        this.gustaBici = gustaBici;
        this.escalarEverest = escalarEverest;
        this.bucearConTiburones = bucearConTiburones;
        this.leerElQuijote = leerElQuijote;
    }

    public String getEmail() {
        return email;
    }

    public String getNickName() {
        return nickName;
    }

    public Boolean getEsHombre() {
        return esHombre;
    }


    public Date getFechaNac() {
        return fechaNac;
    }


    public String getCiudad() {
        return ciudad;
    }

    public Boolean getGustaMusicaBacalao() {
        return gustaMusicaBacalao;
    }

    public Boolean getGustaPaella() {
        return gustaPaella;
    }

    public Boolean getGustaBici() {
        return gustaBici;
    }

    public Boolean getEscalarEverest() {
        return escalarEverest;
    }

    public Boolean getBucearConTiburones() {
        return bucearConTiburones;
    }

    public Boolean getLeerElQuijote() {
        return leerElQuijote;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setEsHombre(Boolean esHombre) {
        this.esHombre = esHombre;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setGustaMusicaBacalao(Boolean gustaMusicaBacalao) {
        this.gustaMusicaBacalao = gustaMusicaBacalao;
    }

    public void setGustaPaella(Boolean gustaPaella) {
        this.gustaPaella = gustaPaella;
    }

    public void setGustaBici(Boolean gustaBici) {
        this.gustaBici = gustaBici;
    }

    public void setEscalarEverest(Boolean escalarEverest) {
        this.escalarEverest = escalarEverest;
    }

    public void setBucearConTiburones(Boolean bucearConTiburones) {
        this.bucearConTiburones = bucearConTiburones;
    }

    public void setLeerElQuijote(Boolean leerElQuijote) {
        this.leerElQuijote = leerElQuijote;
    }

}
