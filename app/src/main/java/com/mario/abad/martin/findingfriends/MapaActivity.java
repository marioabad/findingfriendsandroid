package com.mario.abad.martin.findingfriends;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class MapaActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleApiClient.OnConnectionFailedListener{

    private FirebaseAuth mAuth;

    private List<Boolean> misGustos = new ArrayList<>();
    private List<Boolean> misMetas = new ArrayList<>();

    private static final String TAG = "MAPA";
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;

    private GoogleMap miMapa;

    private List<List<Object>> datosPins = new ArrayList<>();

    private  ArrayList<List<String>> listaInfo = new ArrayList<>();


    MiAdaptador arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        DatosPerfil perfil = (DatosPerfil) getIntent().getSerializableExtra("miperfil");

        misGustos.add(perfil.getGustaMusicaBacalao());
        misGustos.add(perfil.getGustaPaella());
        misGustos.add(perfil.getGustaBici());

        misMetas.add(perfil.getEscalarEverest());
        misMetas.add(perfil.getBucearConTiburones());
        misMetas.add(perfil.getLeerElQuijote());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mAuth = FirebaseAuth.getInstance();
        ListView lv = findViewById(R.id.lista);

        startCapturingLocation();

        List<String> filasLista1 = new ArrayList<>();
        List<String> filasLista2 = new ArrayList<>();
        List<String> filasLista3 = new ArrayList<>();

        filasLista1.add(getString(R.string.nicknameLista));
        filasLista1.add("...");
        filasLista2.add(getString(R.string.gustosLista));
        filasLista2.add("...");
        filasLista3.add(getString(R.string.metasLista));
        filasLista3.add("...");


        listaInfo.add(filasLista1);
        listaInfo.add(filasLista2);
        listaInfo.add(filasLista3);


        arrayAdapter = new MiAdaptador(this, listaInfo);
        lv.setAdapter(arrayAdapter);

    }


    public List<Object> matrizPines(HashMap<String, String> datos, List<Boolean> gustos, List<Boolean> metas) {

        List<Object>elementosDatosPins = new ArrayList<>();

        elementosDatosPins.add(datos);
        elementosDatosPins.add(gustos);
        elementosDatosPins.add(metas);

        return elementosDatosPins ;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        miMapa = googleMap;
        configuraMapa();

    }

    public void configuraMapa(){

        //Cargamos los datos de los usuarios conectados
        FirebaseDatabase basededatos = FirebaseDatabase.getInstance();
        DatabaseReference usuariosConectados = basededatos.getReference("usersConnected");
        final DatabaseReference datosUsuariosConectados = basededatos.getReference("users");


        usuariosConectados.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final HashMap<String, HashMap<String, String>> usuariosConectadosValue = (HashMap<String, HashMap<String, String>>) dataSnapshot.getValue();

                Iterator it = usuariosConectadosValue.entrySet().iterator();
                while (it.hasNext()) {

                    final List<Boolean> gustos = new ArrayList<>();
                    final List<Boolean> metas = new ArrayList<>();


                    final Map.Entry pair = (Map.Entry)it.next();

                    if (!pair.getKey().equals(mAuth.getCurrentUser().getUid())) {
                        datosUsuariosConectados.child(pair.getKey().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                HashMap<String, Object> datosUsuariosConectadosValue = (HashMap<String, Object>) dataSnapshot.getValue();

                                gustos.add( (Boolean) datosUsuariosConectadosValue.get("bacalao_music"));
                                gustos.add( (Boolean) datosUsuariosConectadosValue.get("paella"));
                                gustos.add( (Boolean) datosUsuariosConectadosValue.get("montar_bici"));

                                metas.add( (Boolean) datosUsuariosConectadosValue.get("escalar_everest"));
                                metas.add( (Boolean) datosUsuariosConectadosValue.get("bucear_tiburones"));
                                metas.add( (Boolean) datosUsuariosConectadosValue.get("leer_quijote"));

                                Log.e(TAG, "Gustos: " + gustos);
                                Log.e(TAG, "El valor de usuarios conectados es: " + datosUsuariosConectadosValue);

                                //datosGustosYMetas.add(matrizPines(datos, gustos, metas));
                                HashMap<String, String> datos = new HashMap<>();

                                datos = (HashMap<String, String>) pair.getValue();
                                datosPins.add(matrizPines(datos, gustos, metas));
                                Log.e(TAG, "Datos pins: " + datosPins);

                                ponerPins();

                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }

                    Log.e(TAG, "Value is: " + pair.getKey() + " = " + pair.getValue());
                    it.remove(); // avoids a ConcurrentModificationException
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Error");
            }
        });


        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(40.4169514,-3.7057225));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo((float) 12.0);
        miMapa.moveCamera(center);
        miMapa.animateCamera(zoom);
        miMapa.setOnMarkerClickListener(this);

        /*
        miMapa.addMarker(new MarkerOptions()
                .position(new LatLng(49.89460, -98.22871))
                .title("Hey qué pacha")
                .snippet("Qué tal amigos como estáis?")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marca_mapa))
        );
        */

        if (checkPermission()){
            miMapa.setMyLocationEnabled(true);
        } else {
            requestPermissions();
        }
    }

    void ponerPins(){

        if (datosPins.size() > 0) {

            for (int i = 0; i<datosPins.size(); i++) {

                HashMap<String, String> positionYNick = (HashMap<String, String>) datosPins.get(i).get(0);

                String nick = positionYNick.get("nickname");
                String posicion = positionYNick.get("position");
                String[] parts = posicion.split(",");
                String latit = parts[0];
                String longit = parts[1];
                double latitud = Double.parseDouble(latit);
                double longitud = Double.parseDouble(longit);

                List<Boolean> gustos = new ArrayList<>();
                List<Boolean> metas = new ArrayList<>();

                gustos = (List<Boolean>) datosPins.get(i).get(1);
                metas = (List<Boolean>) datosPins.get(i).get(2);

                List<List<Boolean>> gustosYMetas = new ArrayList<>();

                gustosYMetas.add(gustos);
                gustosYMetas.add(metas);


                Log.e(TAG, "Desde ponerPins: " + datosPins);
                Log.e(TAG, "Desde ponerPins, elementos: " + (datosPins.get(i).get(0)));
                Log.e(TAG, "Desde ponerPins, Position: " + positionYNick.get("position"));

                miMapa.addMarker(new MarkerOptions()
                                .position(new LatLng(latitud, longitud))
                                .title(nick)
                                .snippet("¡Buscando amigos!")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marca_mapa)))
                .setTag(gustosYMetas);

            }
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        if (!marker.getTitle().equals("My Location")) {

            List<List<Boolean>> gustosYMetas = new ArrayList<>();

            gustosYMetas = (List<List<Boolean>>) marker.getTag();

            List<Boolean> gustos = new ArrayList<>();
            List<Boolean> metas = new ArrayList<>();

            gustos = gustosYMetas.get(0);
            metas = gustosYMetas.get(1);

            String gustosCoinc = buscaCoinc(gustos, "Gustos");
            String metasCoinc = buscaCoinc(metas, "Metas");

            List<String> filasLista1 = new ArrayList<>();
            List<String> filasLista2 = new ArrayList<>();
            List<String> filasLista3 = new ArrayList<>();

            filasLista1.add(getString(R.string.nicknameLista));
            filasLista1.add(marker.getTitle());
            filasLista2.add(getString(R.string.gustosLista));
            filasLista2.add(gustosCoinc);
            filasLista3.add(getString(R.string.metasLista));
            filasLista3.add(metasCoinc);

            listaInfo.set(0, filasLista1);
            listaInfo.set(1, filasLista2);
            listaInfo.set(2, filasLista3);
            arrayAdapter.notifyDataSetChanged();
        }

        return false;
    }

    private String buscaCoinc(List<Boolean> lista, String listaAComparar){
        String resultadoString = "";
        for (int i=0; i < lista.size(); i++){
            if (listaAComparar.equals("Gustos")) {
                if (lista.get(i) && lista.get(i).equals(misGustos.get(i))) {
                    resultadoString += Categorias.gustos[i] + " ";
                }
            } else {
                if (lista.get(i) && lista.get(i).equals(misMetas.get(i))) {
                    resultadoString += Categorias.metas[i] + " ";
                }
            }
        }

        return resultadoString;
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void startCapturingLocation(){
        // Iniciamos la captura de posición

        if (checkPermission()){

            LocationManager locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,0,locationListener);

        } else {
            requestPermissions();
        }
    }

    // Comprobamos que tiene permisos para localización
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private void requestPermissions(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

            Toast.makeText(this,"La aplicación necesita permisos de GPS para obtener tu posición",Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, "Permiso concedido, ahora tus amigos te pueden encontrar",Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(this, "Permiso denegado, tus amigos no te pueden encontrar",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    LocationListener locationListener = new LocationListener() {

        public void onLocationChanged(Location location) {
            String msg = "Te hemos localizado: longitud: %s latitud: %s altitud: %s";

            double dLatitude = location.getLatitude();
            double dLongitude = location.getLongitude();
            miMapa.addMarker(new MarkerOptions().position(new LatLng(dLatitude, dLongitude))
                    .title("My Location").icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            //miMapa.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 8));
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        public void onProviderEnabled(String provider) {
            Toast.makeText(MapaActivity.this, "Proveedor Habilitado", Toast.LENGTH_SHORT).show();
        }

        public void onProviderDisabled(String provider) {
            Toast.makeText(MapaActivity.this, "Proveedor Deshabilitado", Toast.LENGTH_SHORT).show();
        }
    };


}
